%global glib_version 2.75.1
%global gtk3_version 3.19.8
%global gtk4_version 4.0.0
%global gsettings_desktop_schemas_version 47~beta
%global libinput_version 1.19.0
%global pipewire_version 0.3.33
%global lcms2_version 2.6
%global colord_version 1.4.5
%global libei_version 1.0.901
%global mutter_api_version 15

%global tarball_version %%(echo %{version} | tr '~' '.')

Name:          mutter
Version:       47.5
Release:       %autorelease
Summary:       Window and compositing manager based on Clutter

License:       GPLv2+
URL:           http://www.gnome.org
Source0:       http://download.gnome.org/sources/%{name}/47/%{name}-%{tarball_version}.tar.xz

# Work-around for OpenJDK's compliance test
Patch:         0001-window-actor-Special-case-shaped-Java-windows.patch

# https://bugzilla.redhat.com/show_bug.cgi?id=1936991
Patch:         0001-Test-deny-atomic-KMS-for-tegra-RHBZ-1936991.patch

# https://pagure.io/fedora-workstation/issue/79
Patch:         0001-place-Always-center-initial-setup-fedora-welcome.patch

# https://bugzilla.redhat.com/show_bug.cgi?id=2239128
# https://gitlab.gnome.org/GNOME/mutter/-/issues/3068
# not upstreamed because for upstream we'd really want to find a way
# to fix *both* problems
Patch:         0001-Revert-x11-window-Compare-input-shape-to-client-rect.patch
Patch:         0002-Revert-x11-window-Update-comment-and-variable-name-t.patch
Patch:         0003-Revert-x11-window-Use-correct-bounding-rect-to-deter.patch

# Revert deprecation fix to avoid newer glib requirement
Patch:         0001-Revert-Replace-deprecated-g_qsort_with_data-with-g_s.patch

# RHEL-74359
Patch:         0001-cursor-renderer-native-Pass-destination-format-to-sc.patch
Patch:         0002-cursor-renderer-native-Store-formats-in-MetaCursorRe.patch
Patch:         0003-cursor-renderer-native-Probe-formats-supported-by-cu.patch
Patch:         0001-cursor-renderer-native-Skip-init_hw_cursor_support_f.patch
Patch:         0001-cursor-renderer-native-Fix-crash-with-MUTTER_DEBUG_D.patch
Patch:         0001-cursor-renderer-native-Cast-MetaGpu-to-MetaGpuKms-on.patch

# RHEL-62220
# DRM lease configuration via monitors.xml and D-Bus:
# https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/4112
Patch:         0001-monitor-manager-Add-forlease-config-to-monitors.xml.patch
Patch:         0002-monitor-Keep-track-of-the-for-lease-status.patch
Patch:         0003-output-kms-Add-meta_output_kms_from_kms_connector.patch
Patch:         0004-kms-connector-Rename-meta_kms_connector_is_for_lease.patch
Patch:         0005-native-drm-lease-Handle-monitors-configured-for-leas.patch
# https://gitlab.gnome.org/GNOME/mutter/-/merge_requests/4121
Patch:         0001-monitor-manager-Return-for-lease-status-in-GetCurren.patch
Patch:         0002-monitor-manager-Allow-to-check-if-config-has-a-visib.patch
Patch:         0003-monitor-manager-Configure-for-lease-monitors-in-Appl.patch

# Backports from gnome-47 branch between 47.5 and 47.6
Patch:         0001-wayland-Fix-refresh-interval-reporting-in-presentati.patch
Patch:         0002-input-capture-session-Disconnect-on_keymap_changed-o.patch

BuildRequires: pkgconfig(gobject-introspection-1.0) >= 1.41.0
BuildRequires: pkgconfig(sm)
BuildRequires: pkgconfig(libwacom)
BuildRequires: pkgconfig(x11)
BuildRequires: pkgconfig(xdamage)
BuildRequires: pkgconfig(xext)
BuildRequires: pkgconfig(xfixes)
BuildRequires: pkgconfig(xi)
BuildRequires: pkgconfig(xrandr)
BuildRequires: pkgconfig(xrender)
BuildRequires: pkgconfig(xcursor)
BuildRequires: pkgconfig(xcomposite)
BuildRequires: pkgconfig(x11-xcb)
BuildRequires: pkgconfig(xkbcommon)
BuildRequires: pkgconfig(xkbcommon-x11)
BuildRequires: pkgconfig(xkbfile)
BuildRequires: pkgconfig(xtst)
BuildRequires: mesa-libEGL-devel
BuildRequires: mesa-libGLES-devel
BuildRequires: mesa-libGL-devel
BuildRequires: mesa-libgbm-devel
BuildRequires: pkgconfig(glesv2)
BuildRequires: pkgconfig(graphene-gobject-1.0)
BuildRequires: pam-devel
BuildRequires: pkgconfig(libdisplay-info)
BuildRequires: pkgconfig(libpipewire-0.3) >= %{pipewire_version}
BuildRequires: pkgconfig(sysprof-capture-4)
BuildRequires: sysprof-devel
BuildRequires: pkgconfig(libsystemd)
BuildRequires: pkgconfig(xkeyboard-config)
BuildRequires: desktop-file-utils
BuildRequires: cvt
# Bootstrap requirements
BuildRequires: gettext-devel git-core
BuildRequires: pkgconfig(libcanberra)
BuildRequires: gsettings-desktop-schemas-devel >= %{gsettings_desktop_schemas_version}
BuildRequires: pkgconfig(gnome-settings-daemon)
BuildRequires: meson
BuildRequires: pkgconfig(gbm)
BuildRequires: pkgconfig(gnome-desktop-4)
BuildRequires: pkgconfig(gudev-1.0)
BuildRequires: pkgconfig(libdrm)
BuildRequires: pkgconfig(libstartup-notification-1.0)
BuildRequires: pkgconfig(wayland-protocols)
BuildRequires: pkgconfig(wayland-server)
BuildRequires: pkgconfig(lcms2) >= %{lcms2_version}
BuildRequires: pkgconfig(colord) >= %{colord_version}
BuildRequires: pkgconfig(libei-1.0) >= %{libei_version}
BuildRequires: pkgconfig(libeis-1.0) >= %{libei_version}

BuildRequires: pkgconfig(libinput) >= %{libinput_version}
BuildRequires: pkgconfig(xwayland)

BuildRequires: python3-dbusmock

Requires: control-center-filesystem
Requires: gsettings-desktop-schemas%{?_isa} >= %{gsettings_desktop_schemas_version}
Requires: gnome-settings-daemon
Requires: gtk4%{?_isa} >= %{gtk4_version}
Requires: libinput%{?_isa} >= %{libinput_version}
Requires: pipewire%{_isa} >= %{pipewire_version}
Requires: startup-notification
Requires: dbus

# Need common
Requires: %{name}-common = %{version}-%{release}

Recommends: mesa-dri-drivers%{?_isa}

Provides: firstboot(windowmanager) = mutter

# Cogl and Clutter were forked at these versions, but have diverged
# significantly since then.
Provides: bundled(cogl) = 1.22.0
Provides: bundled(clutter) = 1.26.0

Conflicts: mutter < 46~beta

# Make sure dnf updates gnome-shell together with this package; otherwise we
# might end up with broken gnome-shell installations due to mutter ABI changes.
Conflicts: gnome-shell < 46~rc

%description
Mutter is a window and compositing manager that displays and manages
your desktop via OpenGL. Mutter combines a sophisticated display engine
using the Clutter toolkit with solid window-management logic inherited
from the Metacity window manager.

While Mutter can be used stand-alone, it is primarily intended to be
used as the display core of a larger system such as GNOME Shell. For
this reason, Mutter is very extensible via plugins, which are used both
to add fancy visual effects and to rework the window management
behaviors to meet the needs of the environment.

%package common
Summary: Common files used by %{name} and forks of %{name}
BuildArch: noarch
Conflicts: mutter < 46~beta

%description common
Common files used by Mutter and soft forks of Mutter

%package devel
Summary: Development package for %{name}
Requires: %{name}%{?_isa} = %{version}-%{release}
# for EGL/eglmesaext.h that's included from public cogl-egl-defines.h header
Requires: mesa-libEGL-devel

%description devel
Header files and libraries for developing Mutter plugins. Also includes
utilities for testing Metacity/Mutter themes.

%package  tests
Summary:  Tests for the %{name} package
Requires: %{name}-devel%{?_isa} = %{version}-%{release}
Requires: %{name}%{?_isa} = %{version}-%{release}
Requires: gtk3%{?_isa} >= %{gtk3_version}

%description tests
The %{name}-tests package contains tests that can be used to verify
the functionality of the installed %{name} package.

%prep
%autosetup -S git -n %{name}-%{tarball_version}

%build
%meson -Degl_device=true
%meson_build

%install
%meson_install

%find_lang %{name}

%files -f %{name}.lang
%license COPYING
%doc NEWS
%{_bindir}/mutter
%{_libdir}/lib*.so.*
%{_libdir}/mutter-%{mutter_api_version}/
%{_libexecdir}/mutter-restart-helper
%{_libexecdir}/mutter-x11-frames
%{_mandir}/man1/mutter.1*

%files common
%{_datadir}/GConf/gsettings/mutter-schemas.convert
%{_datadir}/glib-2.0/schemas/org.gnome.mutter.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.mutter.wayland.gschema.xml
%{_datadir}/gnome-control-center/keybindings/50-mutter-*.xml
%{_udevrulesdir}/61-mutter.rules

%files devel
%{_includedir}/*
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/*

%files tests
%{_libexecdir}/installed-tests/mutter-%{mutter_api_version}
%{_datadir}/installed-tests/mutter-%{mutter_api_version}
%{_datadir}/mutter-%{mutter_api_version}/tests

%changelog
%autochangelog
